<!--侧边栏开始-->
<div id="header">
<div class="navbar-header clearfix">
    <a href="<?php echo $site->url() ?>" class="navbar-brand">
        <img src="<?php echo $site->url() ?>/assets/images/logo.png" alt=""/></a>
</div>
<button id="navbar-toggle" class="navbar-toggle">toggle</button>
<div class="navbar-collapse">
    <ul class="navbar-nav">
        <?php foreach ($pages->visible() as $p): ?>
            <li <?php e($p->isOpen(), 'class="main active"', 'class="main"')?>>
            <div class="title">
                <a class="en" href="<?php echo ($p->link()->empty())?$p->url():$p->link(); ?>" <?php echo ($p->link()->empty())?'':'target="_blank"' ?> ><?php echo html($p->title()) ?></a>
                <a class="cn" href="<?php echo ($p->link()->empty())?$p->url():$p->link(); ?>" <?php echo ($p->link()->empty())?'':'target="_blank"' ?> ><?php echo html($p->title()) ?></a>
            </div>

            <?php if ($p->hasVisibleChildren() && $p->template()!='news'): ?>
                <ul class="sub">
                    <?php foreach ($p->children()->visible() as $p): ?>
                        <li>
                            <a href="<?php echo ($p->link()->empty())?$p->url():$p->link() ?>" <?php echo ($p->link()->empty())?'':'target="_blank"' ?> ><?php echo html($p->title()) ?></a>
                        </li>
                    <?php endforeach ?>
                </ul>
            <?php endif ?>
            </li>
        <?php endforeach ?>
    </ul>

    <!-- 二维码+版权 -->
    <?php $setting=$site->pages()->find('setting'); ?>
    <div class="navbar-bottom">
        <div class="qr-code">
            <img src="<?php echo $site->url() ?>/assets/images/qr-code.png" alt="qrcode"/></div>
        <div class="follow">
            <a href="<?php echo $setting->weibo(); ?>" target="_blank">
                <img src="<?php echo $site->url() ?>/assets/images/follow-sina.png" alt="sina"/></a>
            <a href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo $setting->qq() ?>&site=qq&menu=yes" target="_blank">
                <img src="<?php echo $site->url() ?>/assets/images/follow-qq.png" alt="sina"/></a>
        </div>
        <div class="copyright">
            <p>
                <?php echo $site->copyright()->kirbytext() ?></p>
        </div>
    </div>
    <!-- 二维码+版权结束 -->
</div>

</div>
<!--侧边栏结束  -->