

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php echo $site->title()->html() ?> | <?php echo $page->title()->html() ?></title>
    <meta name="description" content="<?php echo $site->description()->html() ?>">
    <meta name="keywords" content="<?php echo $site->keywords()->html() ?>">
  <?php echo css('assets/css/style.css') ?>
    <!--[if IE 8]>
    <?php echo css('assets/css/IEfixed.css') ?>
    <![endif]-->
    <?php echo css('assets/css/green.css') ?>

    <?php 
        $template = $page->template();
        if($template=='gallery'){
            echo css('assets/js/fancybox/source/jquery.fancybox.css');
            echo css('assets/js/fancybox/source/helpers/jquery.fancybox-buttons.css');
        }
    ?>

    <style type="text/css">
        <?php $setting=$site->pages()->find('setting'); ?>
        #header {
            background-color: <?php echo $setting->navBGColor() ?>;
        }
        .navbar-nav .main:hover .title {
          background: <?php echo $setting->navBGOverColor() ?>;
        }
        .navbar-nav .main a {
            color: <?php echo $setting->navFontColor() ?>;
        }
        .navbar-nav .main.active .title a {
            color: <?php echo $setting->navFontOverColor() ?>;
        }
        a{
            color: <?php echo $setting->lightColor() ?>;
        }
        
    </style>
</head>
<body>
<div class="container">

    <?php snippet('menu') ?>


<div id="content">