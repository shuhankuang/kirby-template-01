﻿<?php snippet('header') ?>


<?php 

include './site/snippets/bg.php';

?>

<div class="content-inner" <?php echo $bgStyle; ?> >
            <!--关于我们开始-->
            <div class="block block-about">
                <div class="about-thumb" style="padding:40px;padding-bottom:0">
                	<?php 
                	    $fileName = substr($page->bannerimage(), 0, $page->bannerimage()->length()-4);
                	?>
                    <img src="<?php echo $page->files()->findBy('name', $fileName)->url() ?>" alt=""/>
                </div>
                <div class="about-content editor-format">
                    <?php echo $page->text()->kt() ?>
                </div>
            </div>
            <!--关于我们结束-->
        </div>

<?php snippet('footer') ?>