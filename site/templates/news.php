﻿
<?php snippet('header') ?>
<?php 

include './site/snippets/bg.php';

?>

<div class="content-inner" <?php echo $bgStyle; ?>>
            <!--新闻列表开始-->
            <div class="block block-news">
                <?php $subpages = $page->children()->visible()->paginate(10); ?>
                <?php $pagination = $subpages->pagination(); ?>
            	<?php foreach ($subpages as $p): ?>
            		
                <?php
                	$image = $site->url()."/content/".$p->diruri()."/".$p->img();
                	$year = date('Y', $p->date());
                	$month = date('m', $p->date());
                	$day = date('d', $p->date());
                ?>
            	
                <div class="news-item">
                    <div class="news-thumb" style="background-image: url(<?php echo $image ?>)">
                        <a href="<?php echo $p->url() ?>" class="mask"></a>
                    </div>
                    <div class="news-time">
                        <span class="year"><?php echo $year ?></span>
                        <span class="date"><?php echo $month.'月'.$day.'日' ?></span>
                    </div>
                    <p>&nbsp;</p>

                    <span class="desc">
                	<?php echo $p->text()->short(125) ?></span>
                    <p>
                    	<a href="<?php echo $p->url() ?>" class="more">继续阅读 &gt;</a>
                    </p>
                </div>
                <?php endforeach ?>



                <div class="page clearfix">
                    <?php for ($i=1; $i < $pagination->pages()+1 ; $i++) {  ?>
                    <?php $currentPage = $pagination->page() ?>
                    <a href="<?php echo $pagination->pageUrl($i) ?>" class="<?php echo ($currentPage == $i)?'active':'' ?>"><?php echo $i ?></a>
                    <?php } ?>    
                </div>
            </div>
            <!--新闻列表结束-->
        </div>

<?php snippet('footer') ?>
