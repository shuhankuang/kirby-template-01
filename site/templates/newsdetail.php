<?php snippet('header') ?>

<?php 

include './site/snippets/bg.php';

?>

<div class="content-inner" <?php echo $bgStyle; ?>>
            <!--新闻内容开始-->
            <?php 
                $image = $site->url()."/content/".$page->diruri()."/".$page->img();
                $year = date('Y', $page->date());
                $month = date('m', $page->date());
                $day = date('d', $page->date());
             ?>
            <div class="block block-news">
                <div class="news-item">
                    <div class="news-thumb" style="background-image: url(<?php echo $image ?>)">
                        <!-- <div class="mask"></div> -->
                    </div>
                    <div class="news-time">
                        <span class="year"><?php echo $year ?></span>
                        <span class="date"><?php echo $month ?>月<?php echo $day ?>日</span>
                    </div>

                    <p>&nbsp;</p>

                    <div class="desc editor-format" style="max-height:none;">
                    <?php echo $page->text()->kt() ?></div>
                </div>

                <div class="share">
                    <!-- JiaThis Button BEGIN -->
                    <div class="jiathis_style_32x32">
                        <a class="jiathis_button_qzone"></a>
                        <a class="jiathis_button_tsina"></a>
                        <a class="jiathis_button_tqq"></a>
                        <a class="jiathis_button_weixin"></a>
                        <a class="jiathis_button_renren"></a>
                        <a href="http://www.jiathis.com/share" class="jiathis jiathis_txt jtico jtico_jiathis" target="_blank"></a>
                    </div>
                    <script type="text/javascript" src="http://v3.jiathis.com/code/jia.js?uid=1368261779228414" charset="utf-8"></script>
                    <!-- JiaThis Button END -->
                </div>

            </div>
            <!--新闻内容结束-->

<?php snippet('footer') ?>
        </div>