<?php snippet('header') ?>

<!--画廊一 扩展版开始-->
        <div id="slider-3" class="slider-3">
            <ul class="slider-list">
				<?php foreach ($page->children()->visible() as $p): ?>

                <li class="slider-item">
                	<?php $cover = $site->url()."/content/".$p->diruri()."/".$p->cover(); ?>
                    <div class="thumb" style="background-image: url(<?php echo $cover ?>)"></div>
                    <div class="info"><?php echo $p->text()->kt() ?></div>
                    <a href="<?php echo $p->url() ?>" class="mask"></a>
                </li>
				<?php endforeach ?>
            </ul>
            <ul class="slider-nav">
                <li class="slider-nav-item prev">
                    <span class="arrow"></span>
                </li>
                <li class="slider-nav-item next">
                    <span class="arrow"></span>
                </li>
            </ul>
        </div>
        <!--画廊一 扩展版结束-->

<?php snippet('footer') ?>