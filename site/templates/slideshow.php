<?php snippet('header') ?>

<!--画廊一开始-->
<div id="slider-1" class="slider-1">
    <ul class="slider-list">

    	<?php foreach ($page->images() as $image): ?>
    	<?php if ($image->hide()->text() != '1'): ?>
        <li class="slider-item">
            <div class="thumb" style="background-image: url(<?php echo $image->url() ?>)"></div>
            <div class="bg"></div>
            <div class="info">
                <div class="desc editor-format">
					<?php echo $image->desc()->kt() ?>
                </div>
                <a class="link" href="<?php echo $image->link()->html() ?>" target="<?php echo e($image->newwin()=='1','_blank', '_self') ?>"><?php echo $image->txt()->kt() ?></a>
            </div>
        </li>
        <?php endif ?>
        <?php endforeach ?>
    </ul>

</div>
<!--画廊一结束-->
    
<?php snippet('footer') ?>