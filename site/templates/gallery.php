<?php echo snippet('header') ?>

<!-- 3列无缝 -->
<!--画廊一 扩展版开始-->
<div id="slider-4" class="slider-4 col-3">
    <ul class="slider-list" id="slider-list">
    </ul>
    <ul id="temp" class="temp hide">
    	<?php foreach ($page->images() as $image): ?>
        <li class="slider-item">
            <div class="inner">
                <a class="fancybox" rel="group" href="<?php echo $image->url() ?>" title="<?php echo $image->intro()->kt() ?>">
                <div class="thumb">
                    <img src="<?php echo thumb($image, array('width' => 300, 'crop' => false))->url() ?>" alt=""/>
                </div>
                <div class="info"><?php echo $image->intro()->kt() ?></div>
                <span href="#" class="mask"></span></a>
            </div>
        </li>
        <?php endforeach ?>
    </ul>
</div>
<!--画廊一 扩展版结束-->

<?php echo snippet('footer') ?>