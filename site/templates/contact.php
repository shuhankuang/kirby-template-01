﻿<?php if (param('status')=='go'): ?>
<?php 



$email = email(array(
  'to'      => $pages->find('setting')->toemail(),
  'from'    => get('name').'<'.get('email').'>',
  'subject' => '来自'.$site->url().'的留言',
  'body'    => '电话：'.get('phone').'  \r\n 内容：'.get('content')
));

if($email->send()) {
  echo '1';
} else {
	echo $email->error();
}

?>

<?php else: ?>

<!--  -->
<?php snippet('header') ?>


<?php 

include './site/snippets/bg.php';

?>

<input id="point" style="display:none" value="<?php echo $page->map()->text() ?>">
<div class="content-inner" <?php echo $bgStyle; ?>>
            <!--关于我们开始-->
            <div class="block block-contact">
			    <?php if ($page->showmap()->text() == '1'): ?>
                	<div id="contact-map" class="contact-map" style="height:300px;">

	                </div>
                <?php endif ?>            
                
                <?php if ($page->showimage()->text() == '1'): ?>
                <div>
                	<?php 
					    $img = substr($page->img(), 0, $page->img()->length()-4);
					?>
                	<img style="width:100%" src="<?php echo $page->files()->findBy('name', $img)->url() ?>" alt="">
                </div>
                <?php endif ?>      
                <div class="contact-content">
                    <h5 class="contact-title">联系我们</h5>
                    <div class="editor-format">
                    	<?php echo $page->text()->kt() ?>
                    </div>
                    <h5 class="contact-title">给我们留言</h5>
                    <form id="myform" role="form" method="post" class="clearfix">
                        <div class="left">
                            <div class="form-group">
                                <label for="name">您的昵称&nbsp;<span style="color:red">*</span></label>
                                <input name="name" type="text" class="form-control" id="name" required>
                            </div>
                            <div class="form-group">
                                <label for="email">QQ or Email&nbsp;<span style="color:red">*</span></label>
                                <input name="email" type="text" class="form-control" id="email" required>
                            </div>
                            <div class="form-group">
                                <label for="tel">联系电话</label>
                                <input name="phone" type="text" class="form-control" id="tel">
                            </div>
                        </div>

                        <div class="form-group right">
                            <label for="text">内容&nbsp;<span style="color:red">*</span></label>
                            <textarea name="content" type="text" class="form-control" id="text" required></textarea>
                        </div>
                        
                        <div class="form-bottom">
                            <button id="sendbtn" type="submit" class="btn btn-submit">提交</button>
                            <button style="display:none;" id="sendingbtn" disabled class="btn btn-submit">发送中...</button>
                        </div>

                    </form>
                </div>
            </div>
            <!--关于我们结束-->
        </div>

<?php snippet('footer') ?>
<?php endif ?>