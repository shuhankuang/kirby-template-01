<?php if(!defined('KIRBY')) exit ?>

username: admin
email: admin@allvery.com
password: >
  $2a$10$oVVMxuMkOjdr3Li7foZfpe7uCalm7kDSGRVy8f48vspdLMoQM8bRu
language: zh-CN
role: admin
token: 239a128ca7d3dd3c2ca461d3641611cfc15bfb79
history:
  - setting
  - weibo
  - null
  - news/news-4
  - news/news-3
firstname:
lastname:
passwordconfirmation:
