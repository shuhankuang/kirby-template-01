<?php if(!defined('KIRBY')) exit ?>

title: Site
pages: 
   template:
     - about
     - contact
     - categories
     - gallery
     - slideshow
     - news
     - puretext
     - setting
     - link
fields:
  title:
    label: Title
    type:  text
  author:
    label: Author
    type:  text
    readonly: true
  description:
    label: Description
    type:  textarea
  keywords:
    label: Keywords
    type:  tags
  copyright:
    label: Copyright
    type:  textarea