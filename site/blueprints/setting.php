<?php if(!defined('KIRBY')) exit ?>
<?php //网站设计，颜色，logo，社交媒体等 ?>
title: 网站设置
pages: false
files: true
fields:
  info:
    label: <i class="fa fa-warning"></i>&nbsp;警告
    type: info
    text: >
       此页面为整个网站的设置文件，请不要删除，删除后网站将不能正常运作！
  title:
    label: 页面标题
    type:  text
  logoHeadline:
    label: 网站LOGO
    type: headline
  logo:
    label: LOGO
    type:  text
    icon: image
    help: 如：logo.jpg, logo.png, logo.gif
  contactHeadline:
    label: 联系邮箱
    type: headline
  toemail:
    label: email
    type: email
    help: 用户留言将直接发到此邮箱
    required: true
  colorHeadline:
    label: 网站颜色设置
    type: headline
  styleToggle:
    label: 选择模板样式
    type: radio
    default: black
    options:
      black: 黑色
      white: 白色
  navBGColor:
    label: 导航背景
    type: text
    default: '#000000'
    width: 1/4
    icon: paint-brush
  navBGOverColor:
    label: 导航背景鼠标经过
    type: text
    default: '#000000'
    width: 1/4
    icon: paint-brush
  navFontColor:
    label: 导航文字
    type: text
    default: '#000000'
    width: 1/4
    icon: paint-brush
  navFontOverColor:
    label: 导航文字鼠标经过
    type: text
    default: '#000000'
    width: 1/4
    icon: paint-brush
  lightColor:
    label: 高亮颜色
    type: text
    default: '#000000'
    width: 1/4
    icon: paint-brush
  subColor:
    label: 辅助色
    type: text
    default: '#000000'
    width: 1/4
    icon: paint-brush
  socailHeadline:
    label: 社交媒体
    type: headline
  weibo:
    label: 新浪微博
    type: url
    icon: weibo
  wechat:
    label: 微信
    type: text
    icon: wechat
  tencentWeibo:
    label: 腾讯微博
    type: url
    icon: tencent-weibo
  qq:
    label: QQ
    type: number
    icon: qq
  renren:
    label: 人人网
    type: url
    icon: renren
  

