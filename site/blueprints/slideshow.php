<?php if(!defined('KIRBY')) exit ?>
<?php //幻灯片页面 ?>
title: 幻灯片页面
pages: false
files: true
  fields:
    txt:
      label: 按钮文字
      type: text
    link: 
      label: 跳转链接
      type: text
    newwin:
      type: checkbox
      text: 链接打开新浏览器窗口
    desc:
      label: 详细说明
      type: textarea
    hide:
      type: checkbox
      text: 是否隐藏此图片
fields:
  title:
    label: 页面标题
    type:  text
  displayImages:
    label: 幻灯片图片
    type: text
    icon: image
    help: 请用 "," 隔开每张图片，图片名字不要包含 ","，如：a.jpg, b.jpg, c.jpg
  interval:
    label: 播放间隔
    type: select
    default: 3
    options:
      02: 2秒
      03: 3秒
      04: 4秒
      05: 5秒
      06: 6秒
      07: 7秒
      08: 8秒
      09: 9秒
  text:
    label: Text
    type:  textarea
  line:
    type: line
  bg:
    label: 页面背景图片
    type: text
    icon: image
  bgColor:
    label: 页面背景颜色
    type: text
    icon: paint-brush
    default: '#000000'
    help: 如果设置了背景图片，那么背景颜色将不起作用
    required: true