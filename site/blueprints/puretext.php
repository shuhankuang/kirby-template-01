<?php if(!defined('KIRBY')) exit ?>
<?php //纯文本页面，只有markdown内容，用户自由编写 ?>
title: 纯文本页面
pages: false
files: true
fields:
  title:
    label: 页面标题
    type:  text
  text:
    label: 页面内容
    type:  textarea
  line:
    type: line
  bg:
    label: 页面背景图片
    type: text
    icon: image
  bgColor:
    label: 页面背景颜色
    type: text
    icon: paint-brush
    default: '#000000'
    help: 如果设置了背景图片，那么背景颜色将不起作用
    required: true

