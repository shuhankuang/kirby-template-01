<?php if(!defined('KIRBY')) exit ?>

title: 联系我们页面
pages: 
  template:
    - about
    - puretext
files: true
fields:
  title:
    label: 页面标题
    type:  text
  img:
    label: 配图
    type: text
    icon: image
    width: 1/2
  showimage:
    label: 配图状态
    type: checkbox
    text: 显示
    icon: eye
    width: 1/2
  map:
    label: 地图
    type: text
    icon: map-marker
    help: 请填入地图坐标，点击<a href="http://api.map.baidu.com/lbsapi/getpoint/" target="_blank"> 拾取坐标 </a>
    width: 1/2
  showmap:
    label: 地图状态
    type: checkbox
    text: 显示
    icon: eye
    width: 1/2
  text:
    label: 页面内容
    type:  textarea
    size: large
  line:
    type: line
  bg:
    label: 页面背景图片
    type: text
    icon: image
  bgColor:
    label: 页面背景颜色
    type: text
    icon: paint-brush
    default: '#000000'
    help: 如果设置了背景图片，那么背景颜色将不起作用
    required: true