<?php if(!defined('KIRBY')) exit ?>
<?php //新闻列表 ?>

title: 新闻详细页
pages: false
files: true
fields:
  title:
    label: 新闻标题
    type:  text
  img:
    label: 新闻配图
    type: text
    icon: image
  date:
    label: 新闻日期
    type: date
    required: true
  text:
    label: 新闻详细内容
    type:  textarea
    required: true
  line:
    type: line
  bg:
    label: 页面背景图片
    type: text
    icon: image
  bgColor:
    label: 页面背景颜色
    type: text
    icon: paint-brush
    default: '#000000'
    help: 如果设置了背景图片，那么背景颜色将不起作用
    required: true