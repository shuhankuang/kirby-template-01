<?php if(!defined('KIRBY')) exit ?>
<?php //关于我们 ?>
title: 关于我们页面
pages: 
  template:
    - puretext
    - about
    - contact
files: true
fields:
  title:
    label: 页面标题
    type:  text
  bannerimage:
    label: 配图
    type: text
    icon: image
  text:
    label: 页面内容
    type:  textarea
  line:
    type: line
  bg:
    label: 页面背景图片
    type: text
    icon: image
  bgColor:
    label: 页面背景颜色
    type: text
    icon: paint-brush
    default: '#000000'
    help: 如果设置了背景图片，那么背景颜色将不起作用
    required: true