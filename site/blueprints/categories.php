<?php if(!defined('KIRBY')) exit ?>

title: 图片展示分类页面
pages: true
  template:
    - gallery
files: false
fields:
  title:
    label: 页面标题
    type:  text
  text:
    label: 页面内容
    type:  textarea
  line:
    type: line
  bg:
    label: 页面背景图片
    type: text
    icon: image
    help: 如果背景图片不归入图片展示，可设： background.jpg0，在图片名字后面加 " 0 "
  bgColor:
    label: 页面背景颜色
    type: text
    icon: paint-brush
    default: '#000000'
    help: 如果设置了背景图片，那么背景颜色将不起作用
    required: true