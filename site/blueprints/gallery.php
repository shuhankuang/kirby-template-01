<?php if(!defined('KIRBY')) exit ?>

title: 图片展示列表页面
pages: false
files: true
  fields:
    intro:
      label: 标题
      type: text
  sortable: true
fields:
  title:
    label: 页面标题
    type:  text
  cover:
    label: 封面
    type: text
    icon: image
    required: true
    help: 如：abc.jpg，此封面将不在图片列表中显示。 
  text:
    label: 页面内容
    type:  textarea
  line0:
    type: line
  alignToggle:
    label: 选择展示样式
    type: radio
    default: _0
    options:
      _0: 无缝
      _1: 间距
  rowToggle:
    label: 选择展示列数
    type: radio
    default: _0
    options:
      _0: 3列
      _1: 4列
  line:
    type: line
  bg:
    label: 页面背景图片
    type: text
    icon: image
    help: 如果背景图片不归入图片展示，可设： background.jpg0，在图片名字后面加 " 0 "
  bgColor:
    label: 页面背景颜色
    type: text
    icon: paint-brush
    default: '#000000'
    help: 如果设置了背景图片，那么背景颜色将不起作用
    required: true