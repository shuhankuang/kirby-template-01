<?php if(!defined('KIRBY')) exit ?>
<?php //新闻列表 ?>

title: 新闻列表页
pages: true
  template:
    - newsdetail
files: true
fields:
  title:
    label: 页面标题
    type:  text
  info: 
    label: <i class="fa fa-info-circle"></i>&nbsp;添加新闻
    type: info
    text: >
      点击左边的“ <strong><i class="fa fa-plus-circle"></i>&nbsp;添加</strong> ”按钮，然后添加新闻详细页，就可发表新闻。
  line:
    type: line
  bg:
    label: 页面背景图片
    type: text
    icon: image
  bgColor:
    label: 页面背景颜色
    type: text
    icon: paint-brush
    default: '#000000'
    help: 如果设置了背景图片，那么背景颜色将不起作用
    required: true