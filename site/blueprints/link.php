<?php if(!defined('KIRBY')) exit ?>

title: 外部链接
pages: false
files: false
fields:
  title:
    label: 页面标题
    type:  text
  link:
    label: 链接
    type:  text
    help: 请输入跳转的链接，如：http://baidu.com