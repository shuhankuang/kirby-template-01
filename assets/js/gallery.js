$(function(){
	        var container = document.querySelector('#slider-list');
	        var temp = document.querySelector('#temp');
	        var elems = temp.querySelectorAll('.slider-item');
	        var msnry = new Masonry( container);

	        $.each(elems,function(index,node){
	            setTimeout(function(){
	                container.appendChild( node );
	                // add and lay out newly appended elements
	                msnry.appended( node );
	                msnry.layout();
	            },100*index);

	        });
	    });

$(document).ready(function() {
	$(".fancybox").fancybox({
		openEffect	: 'fade',
		closeEffect	: 'fade',
		index: 1200
	});
});