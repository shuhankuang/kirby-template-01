// 百度地图API功能
    var map = new BMap.Map("contact-map");
    var xpoints = $("#point").val().split(',');
    var point = new BMap.Point(xpoints[0], xpoints[1]);
    map.centerAndZoom(point, 15);
    map.setMapStyle({style:'grayscale'});
    var marker = new BMap.Marker(point);  // 创建标注
    map.addOverlay(marker);               // 将标注添加到地图中
    marker.setAnimation(BMAP_ANIMATION_BOUNCE); //跳动的动画


	// $("#myform").validate();
    $('#sendingbtn').hide();
    // prepare the form when the DOM is ready 
    $(document).ready(function() { 
        var options = { 
            // target:        '#output1',   // target element(s) to be updated with server response 
            beforeSubmit:  before,  // pre-submit callback 
            success:       success,  // post-submit callback 
            error: _error,
     
            // other available options: 
            url:       document.URL+"/status:go",         // override for form's 'action' attribute 
            type:      "post",        // 'get' or 'post', override for form's 'method' attribute 
            //dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
            //clearForm: true        // clear all form fields after successful submit 
            resetForm: false        // reset the form after successful submit 
     
            // $.ajax options can be used here too, for example: 
            //timeout:   3000 
        }; 
     
        // bind form using 'ajaxForm' 
        $('#myform').ajaxForm(options); 
    });
	function _error(xhr, status, error){
		alert('发送失败，请稍后再试。')
	}
    function before(formData, jqForm, options){
      $('#sendbtn').hide();
      $('#sendingbtn').show();
      return true;
    }
    function success(responseText, statusText, xhr, $form){
    	if(responseText==1){
		      $('#sendbtn').show();
		      $('#sendingbtn').hide();
		      alert('发送成功，我们将很快与您联系。')
		  }else{
		  		$('#sendbtn').show();
		      $('#sendingbtn').hide();
		      alert('发送失败，请稍后再试。')
		  }
    }

    